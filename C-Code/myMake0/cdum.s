	.file	"cdum.c"
	.text
	.section	.rodata
.LC0:
	.string	"This is a C Program!\n"
.LC2:
	.string	"%s"
.LC3:
	.string	"%p\n"
.LC4:
	.string	"%d\n"
.LC5:
	.string	"%lu\n"
	.align 8
.LC1:
	.ascii	"The program creates a cha"
	.string	"racter array and stores a message in it. \nNext, it creates a 'pointer' variable and populates this variable with the location of the character array. \nFinally, we send the message, the pointer value, and address of the pointer all to the standard output.\n\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$336, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, %rdi
	call	puts@PLT
	leaq	-304(%rbp), %rax
	leaq	.LC1(%rip), %rdx
	movl	$35, %ecx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	rep movsq
	movq	%rsi, %rdx
	movq	%rdi, %rax
	movzwl	(%rdx), %ecx
	movw	%cx, (%rax)
	leaq	-304(%rbp), %rax
	movq	%rax, -320(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC2(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	movq	-320(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC3(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$10, %edi
	call	putchar@PLT
	leaq	-320(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC3(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	movb	$12, -321(%rbp)
	movq	$13, -312(%rbp)
	movl	$10, %edi
	call	putchar@PLT
	movsbl	-321(%rbp), %eax
	movl	%eax, %esi
	leaq	.LC4(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	movq	-312(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC5(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	subq	%fs:40, %rdx
	je	.L3
	call	__stack_chk_fail@PLT
.L3:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (GNU) 13.2.1 20230801"
	.section	.note.GNU-stack,"",@progbits
