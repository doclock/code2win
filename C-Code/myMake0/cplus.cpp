#include<iostream>
#include<string>
using namespace std;

int main() {
   cout << "This is C++ Program!\n" << endl;
   string message = "The program creates a string variable and stores a message in it. \nNext, it creates a 'pointer' variable and populates this variable with the location of the string variable. \nFinally, we send the message, the pointer value, and address of the pointer all to the standard output.\n\n";
   string* ptr = &message;
   cout << message;
   cout << ptr;
   cout << "\n\n";
   cout << &ptr << endl;
   return 0;
}


