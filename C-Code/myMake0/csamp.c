#include<stdio.h>

int main(){
    printf("This is a C Program!\n\n");
    char message[] = "The program creates a character array and stores a message in it. \nNext, it creates a 'pointer' variable and populates this variable with the location of the character array. \nFinally, we send the message, the pointer value, and address of the pointer all to the standard output.\n\n";
    char *ptr = message;
	printf("%s", message);
	printf("%p\n", ptr);
	printf("\n");
	printf("%p\n", &ptr);
	return 0;
}
