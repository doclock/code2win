#include <stdio.h>
#include <stdint.h>

int main(){
    printf("This is a C Program!\n\n");
    char message[] = "The program creates a character array and stores a message in it. \nNext, it creates a 'pointer' variable and populates this variable with the location of the character array. \nFinally, we send the message, the pointer value, and address of the pointer all to the standard output.\n\n";
    char *ptr = message;
	printf("%s", message);
	printf("%p\n", ptr);
	printf("\n");
	printf("%p\n", &ptr);
	int8_t thgy = 12;
	uint64_t stuff = 13;
	printf("\n");
	printf("%d\n", thgy);
	printf("%lu\n", stuff);
	return 0;
}



