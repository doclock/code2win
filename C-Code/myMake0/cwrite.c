#include<stdio.h>
#include<string.h>

#define MAX 100

int tofu(){
    printf("This is a C Program!\n\n");
    char buffer[MAX];

	const char* myprompt(char str[]){
	    printf(str);
	    str = fgets(buffer, sizeof(buffer), stdin);
	    str [ strcspn (str, "\n")] = 0;
	    return str;
	}

    // Declare a 'file' pointer with  FILE
	FILE *fptr;
	// Open Resource
	fptr = fopen(myprompt("filename>> "), "w");

	// Write Input to File
	fprintf(fptr, myprompt("content>> "));
	fprintf(fptr, "\n");
	// // Close Resource
	fclose(fptr);

    // fflush(stdin);
    // int c;
    // while ((c = getchar()) != '\n' && c != EOF) { };
	return 0;
}
