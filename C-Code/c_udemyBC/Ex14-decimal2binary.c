#include <stdio.h>

void convert(int input){
    char converted[16] =  {'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'} ;
    int count = 0;

    while(input != 0 && count <= 16){
        converted[15-count] = ((input%2)+'0');
        count += 1;
        input = input / 2;
    }
    printf("0b%s\n", converted);
}

void main(){
    int usr_input;
    scanf("%d", &usr_input);
    convert(usr_input);
}
