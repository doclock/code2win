#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float abs_f(float val){
    if(val < 0){
        val*=-1; 
    }
    return val;
}
int abs_i(int val){
    if(val < 0){
        val*=-1; 
    }
    return val;
}

void main(){
    printf("%.3d\n", abs_i(5));
    printf("%.2d\n", abs_i(-3));
    printf("%.1d\n", abs_i(0));
    printf("%.1f\n", abs_f(0.0));
    printf("%.2f\n", abs_f(-4.0));
    printf("%.3f\n", abs_f(6.0));
}












