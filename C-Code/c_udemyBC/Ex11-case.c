#include <stdio.h>
#include <stdlib.h>

void main(){
    int grade; // Case statements may only work with char and int types.
    printf("\nPlease enter a grade: ");
    sscanf("%d",&grade);
    printf("\n");
    switch (grade/10) { // Divide by 10 to produce integral classes out of real numbers.
        case 10:
            printf("A+\n");
            break;
        case 9:
            printf("A\n");
            break;
        case 8:
            printf("B\n");
            break;
        case 7:
            printf("C\n");
            break;
        case 6:
            printf("D\n");
            break;
        default:  // Finally we have to catch the wide class in the default group or populate each group individually
            printf("F\n");
            break;
    }
}
