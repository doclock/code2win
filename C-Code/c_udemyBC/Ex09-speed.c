#include <stdio.h>

int main(){
    int distance = 300;
    int speed = 80;
    int hours = distance / speed;
    float speedInMinutes = speed / 60.0;
    float remainingMinutes = (distance % speed) / speedInMinutes;

    printf("The time from A to B is : %dh:%fm\n", hours, remainingMinutes);
    return 0;
}
