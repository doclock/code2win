#include <stdio.h>

void main(){
    char textChar = 'a';
    char ASCIIChar = 107;
    char hexChar = 0x61;

    printf("Character: %c \n", textChar);
    printf("Decimal ASCII Code: %d \n", textChar);
    printf("Hexadecimal ASCII Code: 0x%X \n", textChar);

    printf("Character: %c \n", ASCIIChar);
    printf("Decimal ASCII Code: %d \n", ASCIIChar);
    printf("Hexadecimal ASCII Code: 0x%X \n", ASCIIChar);

    printf("Character: %c \n", hexChar);
    printf("Decimal ASCII Code: %d \n", hexChar);
    printf("Hexadecimal ASCII Code: 0x%X \n", hexChar);
}
