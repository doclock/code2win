#include <stdio.h>
#include <stdlib.h>

int main() {
    int num1;
    int num2;
    printf("Length of Rect: ");
    scanf("%d", &num1);
    printf("\nWidth of Rect: ");
    scanf("%d", &num2);
    printf("\nArea of Rect is: %f\n",((float)num1 * (float)num2));
    return 0;
}
