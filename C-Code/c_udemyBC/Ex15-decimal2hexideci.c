#include <stdio.h>

void convert(int input){
    char converted[4] =  {'0','0','0','0'} ;
    int count = 0;

    while(input != 0 && count <= 6){
        char current;
        int result = input%16;
        
        //printf("input: %d\n", input);
        //printf("Result: %d\n", result);
        switch (result) {
            case 15:
                converted[3-count] = 'F';
                break;
            case 14:
                converted[3-count] = 'E';
                break;
            case 13:
                converted[3-count] = 'D';
                break;
            case 12:
                converted[3-count] = 'C';
                break;
            case 11:
                converted[3-count] = 'B';
                break;
            case 10:
                converted[3-count] = 'A';
                //printf("gotem\n");
                break;
            default:
                converted[3-count] = (result)+'0';
                break;

        }
        count += 1;
        input = input / 16;
    }
    printf("0x%s\n", converted);
}

void main(){
    int usr_input;
    scanf("%d", &usr_input);
    convert(usr_input);
}
