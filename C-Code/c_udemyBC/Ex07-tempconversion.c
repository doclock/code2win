#include <stdio.h>
#include <stdlib.h>

int main() {
    float temp;
    printf("\nF --> C Temperature Converter\n\n");
    printf("Farenheit Temperature: ");
    scanf("%f", &temp);
    printf("\n");
    printf("Farenheit:%3.2f --> Celsius:%6.3f\n\n", temp, (5.0/9.0)*(temp-32.0));
    return 0;
}
