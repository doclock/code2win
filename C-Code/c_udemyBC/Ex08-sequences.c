#include <stdio.h>
#include <stdlib.h>

int main() {
    float ith, nth, sn;
    int num;
    printf("Enter the Initial Term (ith): ");
    scanf("%f", &ith);
    printf("Enter the Last Term (nth): ");
    scanf("%f", &nth);
    printf("Enter the Number of Elements in the Sequence: ");
    scanf("%d", &num);
    sn = (ith + nth) * num / 2;
    printf("The sum of your sequence = %.2f\n", sn);
    return 0;
}
