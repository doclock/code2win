#include <stdio.h>
#include <stdlib.h>

int main() {
    float num1;
    float num2;
    printf("Length of Rect: ");
    scanf("%f", &num1);
    printf("\nWidth of Rect: ");
    scanf("%f", &num2);
    printf("\nArea of Rect is: %f\n",(num1 * num2));
    return 0;
}
